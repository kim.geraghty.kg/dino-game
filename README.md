Welcome to Save Dino!

## FEATURES

Score points by collecting (touching) the flower as many times as possible.
Stay alive by avoiding the asteroid!

This game features 4 levels. Let me know how far you get! Good luck.

## LINK

To play the game, go here: http://kimsgames.mygamesonline.org/home

## PREVIEW

![Dino game preview](/Dinogame-highscore.jpg "Dino game high score")
